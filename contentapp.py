#!/usr/bin/python3

import webapp


formulario = """
    <br/><br/>
    <form action="" method = "POST"> 
    <input type="text" name="name" value="">
    <input type="submit" value="Enviar">
    </form>
"""


class ContentPostApp(webapp.webApp):

    #  diccionario con el contenido para los recursos disponibles
    contenido = {'/': 'Inicio',
                 '/frio': 'Hace frío',
                 '/calor': 'Hace calor',
                 '/lluvia': 'Está lloviendo',
                 '/sol': 'El sol brilla intensamente'
                 }

    def parse(self, request):
        """Parse the received request, extracting the relevant information."""

        metodo, recurso, _ = request.split(' ', 2)  # Trocea los dos primeros
        # espacios en blanco y guarda las dos primeras partes, el resto lo deshecha

        try:
            cuerpo = request.split('\r\n\r\n', 1)[1]  # Trocea una sola vez por \r\n\r\n y
            # guarda el final para obtener el cuerpo
        except IndexError:
            cuerpo = ""
        return metodo, recurso, cuerpo

    def process(self, parsedRequest):
        """Process the relevant elements of the request."""

        metodo, recurso, cuerpo = parsedRequest
        print('El metodo es: ' + metodo)
        print('El recurso es: ' + recurso)
        print('El cuerpo es: ' + cuerpo)

        if metodo == "POST":  # Guarda lo introducido en el formulario
            # Si el recurso no estaba, se guarda ese contenido en una nueva entrada en el diccionario para ese recurso
            # Si el recurso ya existe, actualiza su contenido en el diccionario
            self.contenido[recurso] = cuerpo.split('=')[1].replace('+', ' ')  # con el metodo replace se sustituyen los
            # caracteres + resultantes de escribir varias palabras espaciadas en el cuerpo por espacios

        print()
        print(self.contenido)

        if recurso in self.contenido.keys():
            httpCode = "200 OK"
            if metodo == "POST":
                htmlBody = "<html><body>Guardado " + self.contenido[recurso] \
                       + " para el recurso " + recurso + formulario + "</body></html>"
            else:
                htmlBody = "<html><body>" + self.contenido[recurso] + formulario + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "<html><body>El recurso " + recurso + " no forma parte de los contenidos." \
                                                             + formulario + "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = ContentPostApp("localhost", 1234)
